import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentComponent } from './appointment/appointment.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { LocateComponent } from './locate/locate.component';
import { PackagesComponent } from './packages/packages.component';
import { SerHomeComponent } from './services/ser-home/ser-home.component';

const routes: Routes = [
  {path:'appointment', component:AppointmentComponent},
  {path:'doctors', component:DoctorListComponent},
  {path:'locate', component:LocateComponent},
  {path:'packages', component:PackagesComponent},
  {path:'services', component:SerHomeComponent},
  {path:'', redirectTo:'/services', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
