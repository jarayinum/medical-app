import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrologyComponent } from './urology.component';

describe('UrologyComponent', () => {
  let component: UrologyComponent;
  let fixture: ComponentFixture<UrologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
