import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastroEnterologyComponent } from './gastro-enterology.component';

describe('GastroEnterologyComponent', () => {
  let component: GastroEnterologyComponent;
  let fixture: ComponentFixture<GastroEnterologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastroEnterologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastroEnterologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
