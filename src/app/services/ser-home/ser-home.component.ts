import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-ser-home',
  templateUrl: './ser-home.component.html',
  styleUrls: ['./ser-home.component.css']
})
export class SerHomeComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
