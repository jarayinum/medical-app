import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerHomeComponent } from './ser-home.component';

describe('SerHomeComponent', () => {
  let component: SerHomeComponent;
  let fixture: ComponentFixture<SerHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
