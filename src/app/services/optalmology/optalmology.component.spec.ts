import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptalmologyComponent } from './optalmology.component';

describe('OptalmologyComponent', () => {
  let component: OptalmologyComponent;
  let fixture: ComponentFixture<OptalmologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptalmologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptalmologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
