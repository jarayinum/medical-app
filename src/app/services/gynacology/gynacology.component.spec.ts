import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GynacologyComponent } from './gynacology.component';

describe('GynacologyComponent', () => {
  let component: GynacologyComponent;
  let fixture: ComponentFixture<GynacologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GynacologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GynacologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
