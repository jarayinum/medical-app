import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeuroSurgeryComponent } from './neuro-surgery.component';

describe('NeuroSurgeryComponent', () => {
  let component: NeuroSurgeryComponent;
  let fixture: ComponentFixture<NeuroSurgeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeuroSurgeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeuroSurgeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
