import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagingCentreComponent } from './imaging-centre.component';

describe('ImagingCentreComponent', () => {
  let component: ImagingCentreComponent;
  let fixture: ComponentFixture<ImagingCentreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagingCentreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagingCentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
