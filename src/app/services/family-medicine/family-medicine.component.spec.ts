import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyMedicineComponent } from './family-medicine.component';

describe('FamilyMedicineComponent', () => {
  let component: FamilyMedicineComponent;
  let fixture: ComponentFixture<FamilyMedicineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyMedicineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyMedicineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
