import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SerHomeComponent } from './services/ser-home/ser-home.component';
import { FamilyMedicineComponent } from './services/family-medicine/family-medicine.component';
import { GynacologyComponent } from './services/gynacology/gynacology.component';
import { UrologyComponent } from './services/urology/urology.component';
import { DentalComponent } from './services/dental/dental.component';
import { DermatologyComponent } from './services/dermatology/dermatology.component';
import { ImagingCentreComponent } from './services/imaging-centre/imaging-centre.component';
import { LaboratoryComponent } from './services/laboratory/laboratory.component';
import { OrthopedicsComponent } from './services/orthopedics/orthopedics.component';
import { PediatricsComponent } from './services/pediatrics/pediatrics.component';
import { PharmacyComponent } from './services/pharmacy/pharmacy.component';
import { PhysiotherapyComponent } from './services/physiotherapy/physiotherapy.component';
import { EntComponent } from './services/ent/ent.component';
import { NeuroSurgeryComponent } from './services/neuro-surgery/neuro-surgery.component';
import { InternalMedicineComponent } from './services/internal-medicine/internal-medicine.component';
import { OptalmologyComponent } from './services/optalmology/optalmology.component';
import { GastroEnterologyComponent } from './services/gastro-enterology/gastro-enterology.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { PackagesComponent } from './packages/packages.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { DoctorDetailsComponent } from './doctors/doctor-details/doctor-details.component';
import { LocateComponent } from './locate/locate.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from './auth.service';

@NgModule({
  declarations: [
    AppComponent,
    SerHomeComponent,
    FamilyMedicineComponent,
    GynacologyComponent,
    UrologyComponent,
    DentalComponent,
    DermatologyComponent,
    ImagingCentreComponent,
    LaboratoryComponent,
    OrthopedicsComponent,
    PediatricsComponent,
    PharmacyComponent,
    PhysiotherapyComponent,
    EntComponent,
    NeuroSurgeryComponent,
    InternalMedicineComponent,
    OptalmologyComponent,
    GastroEnterologyComponent,
    AppointmentComponent,
    PackagesComponent,
    DoctorListComponent,
    DoctorDetailsComponent,
    LocateComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
